package com.example.usuario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.usuario.api.Api;
import com.example.usuario.api.Servicios.ServicioPeticion;
import com.example.usuario.viewmodel.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    //Declaramos la variable que recibira el Token una vez registrados en el Web Service
    private String APITOKEN = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Interface que procesa and modifica los datos de preferencias retornados por Context#getSharedPreference
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if(token != ""){

        }

        //Declaramos las variables de nuestro XML
        final EditText email = findViewById(R.id.editText3);
        final EditText pass = findViewById(R.id.editText4);
        Button loginin = (Button) findViewById(R.id.button2);
        Button signup = (Button) findViewById(R.id.button);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Registro.class));
            }
        });

        //Condicional que le indica al usuario que sus campos necesarios estan vacios.
        loginin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().isEmpty() || email.getText().toString() == ""){
                    email.setSelectAllOnFocus(true);
                    email.requestFocus();
                    return;
                }
                if(pass.getText().toString().isEmpty() || pass.getText().toString() == ""){
                    pass.setSelectAllOnFocus(true);
                    pass.requestFocus();
                    return;
                }

                //Creamos la variable "service" y nuestra Interface que llama a nuesta Api pasandole los datos email y contraseña
                ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall = service.getLogin(email.getText().toString(),pass.getText().toString());
                //Estructura de nuestra peticion en formato JSON, si al llamar a la Api esta devuelve un valor estado "True", permitira guardar el token correspondiente.
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if(peticion.estado == "true"){
                            APITOKEN = peticion.token;
                            guardarPreferencias();
                            Toast.makeText(MainActivity.this,"Datos correctos", Toast.LENGTH_LONG).show();
                            if(APITOKEN != ""){
                                Toast.makeText(MainActivity.this, "token valido", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(MainActivity.this, Home.class));
                            }
                        }
                        else{
                            Toast.makeText(MainActivity.this,"Datos Incorrectos :c", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    //Metodo que permite recibir el token de nuestras preferencias y asignarlo a una variable local para su uso.
    public void guardarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN",token);
        editor.commit();
    }
}
