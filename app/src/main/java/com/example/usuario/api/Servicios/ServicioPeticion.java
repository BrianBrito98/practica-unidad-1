package com.example.usuario.api.Servicios;

import com.example.usuario.viewmodel.Detalle_Usuario;
import com.example.usuario.viewmodel.Peticion_Login;
import com.example.usuario.viewmodel.Peticion_Usuarios;
import com.example.usuario.viewmodel.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> getLogin(@Field("username") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> signupUser(@Field("username") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/todosUsuarios")
    Call<Peticion_Usuarios> getUsuarios(@Field("a") String usuario);

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<Detalle_Usuario> getDetalles(@Field("usuarioId") String detalles);
}
