package com.example.usuario.viewmodel;

public class Detalle_Usuario {
    public String estado;
    public String detalles;
    public String usuario;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public Detalle_Usuario(String detalles){
        this.detalles = detalles;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado){
        this.estado = estado;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles){
        this.detalles = detalles;
    }

}
