package com.example.usuario.viewmodel;

public class Registro_Usuario {
    public String estado;
    public String email;
    public String password;
    public String details;

    public Registro_Usuario(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEstado() { return estado; }

    public void setEstado(String estado) { this.estado = estado; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getDetails() {
        return  details;
    }

    public void setDetails(String details) {
        this.details = details;
    }


}
