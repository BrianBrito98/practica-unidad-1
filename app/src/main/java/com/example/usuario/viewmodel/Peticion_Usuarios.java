package com.example.usuario.viewmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Peticion_Usuarios {
    @SerializedName("estado")
    @Expose
    public String estado;

    @SerializedName("usuarios")
    @Expose
    private List<Usuarios> usuarios;

    public String getStatus() {
        return estado;
    }

    public void setStatus(String estado) {
        this.estado = estado;
    }

    public List<Usuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }
}
