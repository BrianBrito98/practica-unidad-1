package com.example.usuario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.usuario.api.Api;
import com.example.usuario.api.Servicios.ServicioPeticion;
import com.example.usuario.viewmodel.Registro_Usuario;
import com.example.usuario.api.Api;
import com.example.usuario.api.Servicios.ServicioPeticion;
import com.example.usuario.viewmodel.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Definimos nuestras variables XML
        Button regis = (Button) findViewById(R.id.button5);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText Usuario = (EditText) findViewById(R.id.editText);
                EditText password = (EditText) findViewById(R.id.editText6);
                EditText rpassword = (EditText) findViewById(R.id.editText2);

                //Condicional que le indica al usuario que sus campos necesarios estan vacios.
                if(Usuario.getText().toString() == ""){
                    Usuario.setSelectAllOnFocus(true);
                    Usuario.requestFocus();
                    return;
                }
                if(password.getText().toString() == "") {
                    password.setSelectAllOnFocus(true);
                    password.requestFocus();
                    return;
                }
                if(rpassword.getText().toString() == "") {
                    rpassword.setSelectAllOnFocus(true);
                    rpassword.requestFocus();
                    return;
                }
                if(!password.getText().toString().equals(rpassword.getText().toString())) {
                    Toast.makeText(Registro.this, "The passwords entered do not match", Toast.LENGTH_SHORT).show();
                    return;
                }

                //Variable "service" llamada de nuestra interface la cual nos ofrecera la extension al web service necesario, mientras le pasamos los datos para registrar
                //Estos datos son Usuario y contraseña
                ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> signCall = service.signupUser(Usuario.getText().toString(),password.getText().toString());
                signCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                        Registro_Usuario petition = response.body();
                        if(response.body() == null){
                            Toast.makeText(Registro.this, "Ocurrio un error, intentelo de nuevo", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(petition.estado == "true"){
                            //Si la variable JSON devuelve un estado "True", los datos se registraran con exito.
                            startActivity(new Intent(Registro.this, MainActivity.class));
                            Toast.makeText(Registro.this, "Datos registrados con exito", Toast.LENGTH_LONG).show();
                        }
                        else{
                            //En caso contrario, el nombre de usuario ya esta en uso, por lo que se notificara a la persona que use la app.
                            Toast.makeText(Registro.this, "Nombre de usuario ya en uso", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(Registro.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
